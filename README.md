# PytorchBasics



## content:

- class 1

description: converting it to a PyTorch tensor, and shows how a NumPy array and a PyTorch tensor can share memory, and how changes to one affect the other.

`colab:` [Basic of tensors](https://colab.research.google.com/drive/1JB6HaHTpFijHR_6kDF3zgZtpwfE0ITrD?usp=sharing)
